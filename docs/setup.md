# Setup steps
### Warning: this project is for development only. Credentials and configurations are hardcoded.

## 1. Virtualenv

**You must have Python3.6 and virtualenv installed in your machine.** 

Lets configure a Python Virtual Environment for the project

```
virtualenv -p python3.6 fruitshop_venv
``` 
After it is created, activate it.
```
source fruitshop_venv/bin/activate
```

## 2. Dependencies
Next, let's install the project dependencies with `pip`.
```
pip install -r <project_folder>/requirements.txt
```

## 3. Database
**You must have Docker and Docker-compose installed on your machine.** 

Let's configure the database for the project. 

Inside the `docker` folder, you can find a `docker-compose.yml` file, which setups a Postgres 12 database. 
On initialization the database will create the necessary user and database for the project.

```
# Inside the project folder execute the following commands

cd docker
sudo docker-compose up --build
```

This will build the Docker image, run the container and initialize the database. 
Data will be stored in a Docker volume defined in the docker-compose file.

Finally, let's setup the Django tables.

**Make sure you have the virtual environment activated**
```
# Inside the project folder execute the following commands

cd fruitshop
python manage.py migrate
```

This will configure the project tables.

### 4. Run project

Finally, we are ready to run the project.

**Make sure you have the virtual environment activated**

```
# Inside the project folder execute the following commands

cd fruitshop
python manage.py runserver
```

This will run the Django development server on the port 8000. 
You can access the platform by going to:

http://localhost:8000