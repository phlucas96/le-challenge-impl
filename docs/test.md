# Test steps

This guide will show you how to run the tests.

## 1. Setup the project

Follow the steps until at least step 3 in the [setup.md](setup.md) file.

## 2. Run the code linter (optional)

Run the the PEP 8 code linter

**Make sure you have the virtual environment activated**
```
# Inside the project folder execute the following commands
cd fruitshop
pycodestyle .
``` 

## 3. Run the tests

Finally, run the tests:

**Make sure you have the virtual environment activated**
```
# Inside the project folder execute the following commands
cd fruitshop
python manage.py test ../tests -v 2
```