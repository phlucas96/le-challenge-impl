import json
from codecs import encode, decode

import factory
import faker
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from products.factories import ProductFactory
from products.models import Product
from sells.factories import SellFactory
from sells.models import Sell
from users.factories import UserFactory

faker = faker.Faker()


# Sells List
class SellsViewListAuthTestCase(TestCase):

    def test_list_sells_no_auth(self):
        response = self.client.get(reverse('sells-list'))
        self.assertEqual(response.status_code, 302)

    def test_list_sells_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        response = self.client.get(reverse('sells-list'))
        self.assertEqual(response.status_code, 200)


class SellsViewListResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_list_sells_no_sells(self):
        response = self.client.get(reverse('sells-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        sells = list(Sell.objects.all().values())
        self.assertEqual(len(data["results"]), len(sells))

    def test_list_sells_with_sells(self):
        SellFactory.create_batch(10)

        response = self.client.get(reverse('sells-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        sells = list(Sell.objects.all().values())
        self.assertEqual(len(data["results"]), len(sells))


# Sells Create
class SellsViewCreateAuthTestCase(TestCase):

    def test_create_sell_no_auth(self):
        response = self.client.post(reverse('sells-list'))
        self.assertEqual(response.status_code, 302)

    def test_create_sell_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        prod = ProductFactory()
        payload = {
            'user_id': self.user.id,
            'product_id': prod.id,
            'quantity': 12
        }

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 201)


class SellsViewCreateResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_create_sell_all_fields(self):
        prod = ProductFactory()
        payload = {
            'user_id': self.user.id,
            'product_id': prod.id,
            'quantity': 12
        }

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 201)

        data = json.loads(decode(response.content, 'utf-8'))
        sell = Sell.objects.filter(id=data["id"]).first()
        self.assertIsNotNone(sell)

    def test_create_sell_no_data(self):
        payload = {}

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_sell_no_user(self):
        prod = ProductFactory()
        payload = {
            'product_id': prod.id,
            'quantity': 12
        }

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_sell_no_product(self):
        payload = {
            'user_id': self.user.id,
            'quantity': 12
        }

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_sell_no_quantity(self):
        prod = ProductFactory()
        payload = {
            'user_id': self.user.id,
            'product_id': prod.id,
        }

        response = self.client.post(reverse('sells-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)


# Sells Retrieve
class SellsViewRetrieveAuthTestCase(TestCase):

    def test_retrieve_sells_no_auth(self):
        sell = SellFactory()
        response = self.client.get(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 302)

    def test_retrieve_sells_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        sell = SellFactory()
        response = self.client.get(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 200)


class SellsViewRetrieveResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_retrieve_sells_non_existent(self):
        s = SellFactory()
        response = self.client.get(reverse('sells-detail', args=[s.id + 1]))
        self.assertEqual(response.status_code, 404)

    def test_retrieve_sells_exists(self):
        s = SellFactory()
        response = self.client.get(reverse('sells-detail', args=[s.id]))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        sell = Sell.objects.get(pk=s.id)
        self.assertEqual((data["id"]), sell.id)

# Sells Update

class SellsViewUpdateAuthTestCase(TestCase):

    def test_update_sell_no_auth(self):
        sell = SellFactory()
        response = self.client.put(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 302)

    def test_update_sell_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        sell = SellFactory()

        prod = ProductFactory()
        payload = {
            'user_id': self.user.id,
            'product_id': prod.id,
            'quantity': 12
        }

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 200)


class SellsViewUpdateResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_update_sell_all_fields(self):
        sell = SellFactory()
        prod = ProductFactory()
        payload = {
            'user_id': sell.user.id,
            'product_id': prod.id,
            'quantity': sell.quantity
        }

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))
        sell = Sell.objects.filter(id=data["id"]).first()
        self.assertIsNotNone(sell)
        self.assertEqual(sell.product_id, prod.id)

    def test_update_sell_no_data(self):
        sell = SellFactory()
        payload = {}

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)


    def test_update_sell_no_user(self):
        sell = SellFactory()
        prod = ProductFactory()
        payload = {
            'product_id': prod.id,
            'quantity': sell.quantity
        }

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_sell_no_product(self):
        sell = SellFactory()
        payload = {
            'user_id': sell.user.id,
            'quantity': sell.quantity
        }

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_sell_no_quantity(self):
        sell = SellFactory()
        prod = ProductFactory()
        payload = {
            'user_id': sell.user.id,
            'product_id': prod.id,
        }

        response = self.client.put(reverse('sells-detail', args=[sell.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

# Sells Delete

class SellsViewDeleteAuthTestCase(TestCase):

    def test_delete_sell_no_auth(self):
        sell = SellFactory()
        response = self.client.delete(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 302)

    def test_delete_sell_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        sell = SellFactory()
        response = self.client.delete(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 204)


class SellsViewDeleteResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_delete_sell_non_existent(self):
        sell = SellFactory()
        response = self.client.delete(reverse('sells-detail', args=[sell.id + 1]))
        self.assertEqual(response.status_code, 404)

    def test_delete_sell_exists(self):
        sell = SellFactory()
        response = self.client.delete(reverse('sells-detail', args=[sell.id]))
        self.assertEqual(response.status_code, 204)
        se = Sell.objects.filter(id=sell.id).first()
        self.assertEqual(se, None)