import json
from codecs import decode

import faker
from django.test import TestCase
from django.urls import reverse

from products.factories import ProductFactory
from products.models import Product
from users.factories import UserFactory

faker = faker.Faker()


# Products List
class ProductsViewListAuthTestCase(TestCase):

    def test_list_products_no_auth(self):
        response = self.client.get(reverse('products-list'))
        self.assertEqual(response.status_code, 302)

    def test_list_products_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        response = self.client.get(reverse('products-list'))
        self.assertEqual(response.status_code, 200)


class ProductsViewListResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_list_products_no_products(self):
        response = self.client.get(reverse('products-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        products = list(Product.objects.all().values())
        self.assertEqual(len(data["results"]), len(products))

    def test_list_products_with_products(self):
        ProductFactory.create_batch(10)

        response = self.client.get(reverse('products-list'))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        products = list(Product.objects.all().values())
        self.assertEqual(len(data["results"]), len(products))


# Products Create
class ProductsViewCreateAuthTestCase(TestCase):

    def test_create_products_no_auth(self):
        response = self.client.post(reverse('products-list'))
        self.assertEqual(response.status_code, 302)

    def test_create_products_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 201)


class ProductsViewCreateResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_create_products_all_fields(self):
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 201)

        data = json.loads(decode(response.content, 'utf-8'))
        prod = Product.objects.filter(id=data["id"]).first()
        self.assertIsNotNone(prod)

    def test_create_products_no_data(self):
        payload = {}

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_products_no_name(self):
        payload = {
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_products_no_description(self):
        payload = {
            'name': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_products_no_exp_date(self):
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'price': 12
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_create_products_no_price(self):
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '2020-04-11',
        }

        response = self.client.post(reverse('products-list'), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)


# Products Retrieve
class ProductsViewRetrieveAuthTestCase(TestCase):

    def test_retrieve_products_no_auth(self):
        prod = ProductFactory()
        response = self.client.get(reverse('products-detail', args=[prod.id]))
        self.assertEqual(response.status_code, 302)

    def test_retrieve_products_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        prod = ProductFactory()
        response = self.client.get(reverse('products-detail', args=[prod.id]))
        self.assertEqual(response.status_code, 200)


class ProductsViewRetrieveResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_retrieve_products_non_existent(self):
        p = ProductFactory()
        response = self.client.get(reverse('products-detail', args=[p.id + 1]))
        self.assertEqual(response.status_code, 404)

    def test_retrieve_products_exists(self):
        p = ProductFactory()
        response = self.client.get(reverse('products-detail', args=[p.id]))
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))

        prod = Product.objects.get(pk=p.id)
        self.assertEqual((data["id"]), prod.id)


# Products Update

class ProductsViewUpdateAuthTestCase(TestCase):

    def test_update_product_no_auth(self):
        prod = ProductFactory()
        response = self.client.put(reverse('products-detail', args=[prod.id]))
        self.assertEqual(response.status_code, 302)

    def test_update_product_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        prod = ProductFactory()

        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 200)


class ProductsViewUpdateResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_update_product_all_fields(self):
        prod = ProductFactory(name="aaa")
        payload = {
            'name': "bbb",
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(decode(response.content, 'utf-8'))
        prod = Product.objects.filter(id=data["id"]).first()
        self.assertIsNotNone(prod)
        self.assertEqual(prod.name, "bbb")

    def test_update_product_no_data(self):
        prod = ProductFactory()
        payload = {}

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_product_no_name(self):
        prod = ProductFactory()
        payload = {
            'description': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_product_no_description(self):
        prod = ProductFactory()
        payload = {
            'name': faker.name(),
            'exp_date': '2020-04-11',
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_product_no_exp_date(self):
        prod = ProductFactory()
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_product_wrong_format_exp_date(self):
        prod = ProductFactory()
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '11-04-2222',
            'price': 12
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_product_no_price(self):
        prod = ProductFactory()
        payload = {
            'name': faker.name(),
            'description': faker.name(),
            'exp_date': '2020-04-11',
        }

        response = self.client.put(reverse('products-detail', args=[prod.id]), data=payload, content_type='application/json')
        self.assertEqual(response.status_code, 400)


# Products Delete

class ProductsViewDeleteAuthTestCase(TestCase):

    def test_delete_products_no_auth(self):
        prod = ProductFactory()
        response = self.client.delete(reverse('products-detail', args=[prod.id]))
        self.assertEqual(response.status_code, 302)

    def test_delete_products_with_auth(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

        prod = ProductFactory()
        response = self.client.delete(reverse('products-detail', args=[prod.id]))
        self.assertEqual(response.status_code, 204)


class ProductsViewDeleteResultTestCase(TestCase):

    def setUp(self):
        password = faker.password()
        self.user = UserFactory(password=password)
        res_login = self.client.login(username=self.user.username, password=password)
        self.assertTrue(res_login)

    def test_delete_products_non_existent(self):
        p = ProductFactory()
        response = self.client.delete(reverse('products-detail', args=[p.id + 1]))
        self.assertEqual(response.status_code, 404)

    def test_delete_products_exists(self):
        p = ProductFactory()
        response = self.client.delete(reverse('products-detail', args=[p.id]))
        self.assertEqual(response.status_code, 204)
        prod = Product.objects.filter(id=p.id).first()
        self.assertEqual(prod, None)
