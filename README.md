# Fruitshop Platform -  MD3 Challenge

An implementation of a fruitshop platform, a challenge by MD3 https://gitlab.mediatree.fr/snippets/67

By Pedro Lucas, September 2020

## Features

- Users can **register**, **login** and **logout** in the platform;
- Users can **list**, **create**, **update** and **delete** products;
- Users can **list**, **create**, **update** and **delete** sells;
- Only authenticated users can manage products and sells.

## Technologies

- Python 3.6
- Django 2.2.16
- Postgres 12
- Docker and Docker-compose (for the DB)
- Faker and Factory-boy (for testing)
## Setup and run

Follow the steps in the [setup.md](docs/setup.md) file.

## Test

Follow the steps in the [test.md](docs/test.md) file.