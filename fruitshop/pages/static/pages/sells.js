function deleteSell(id) {
    fetch(`/api/sells/${id}/`, {method: 'DELETE'})
        .then(response => {
            console.log(response)
            location.reload();
        })
        .catch(error => console.error(error))
}

function createSell() {
    const user_drop = document.getElementById("id_user")
    const user_id = user_drop.options[user_drop.selectedIndex].value;
    const product_drop = document.getElementById("id_product");
    const product_id = product_drop.options[product_drop.selectedIndex].value;
    const quantity = document.getElementById("id_quantity").value;

    fetch(`/api/sells/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({
            user_id: user_id,
            product_id: product_id,
            quantity: quantity
        })
    })
        .then(response => {
            console.log(response)
            if (response.ok) {
                alert('Sell created successfully');
                window.location.replace('../');
            } else {
                alert("ERROR: invalid sell");
            }
        })
        .catch(error => {
            console.error({"error": error})
            alert("ERROR: " + error)
        })
}

function editSell(id) {
    const user_drop = document.getElementById("id_user")
    const user_id = user_drop.options[user_drop.selectedIndex].value;
    const product_drop = document.getElementById("id_product");
    const product_id = product_drop.options[product_drop.selectedIndex].value;
    const quantity = document.getElementById("id_quantity").value;

    fetch(`/api/sells/${id}/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "PUT",
        body: JSON.stringify({
            user_id: user_id,
            product_id: product_id,
            quantity: quantity
        })
    })
        .then(response => {
            console.log(response)
            if (response.ok) {
                alert('Sell updated successfully');
                window.location.replace('../../');
            } else {
                alert("ERROR: invalid sell");
            }
        })
        .catch(error => {
            console.error({"error": error})
            alert("ERROR: " + error)
        })
}