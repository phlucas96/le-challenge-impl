function deleteProduct(id) {
    fetch(`/api/products/${id}/`, {method: 'DELETE'})
        .then(response => {
            console.log(response)
            location.reload();
        })
        .catch(error => console.error(error))
}

function createProduct() {
    const name = document.getElementById("id_name").value;
    const description = document.getElementById("id_description").value;
    const exp_date = document.getElementById("id_exp_date").value;
    const price = document.getElementById("id_price").value;

    fetch(`/api/products/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({
            name: name,
            description: description,
            exp_date: exp_date,
            price: price
        })
    })
        .then(response => {
            console.log(response)
            if (response.ok) {
                alert('Product created successfully');
                window.location.replace('../');
            } else {
                alert("ERROR: invalid product");
            }
        })
        .catch(error => {
            console.error({"error": error})
            alert("ERROR: " + error)
        })
}

function editProduct(id) {
    const name = document.getElementById("id_name").value;
    const description = document.getElementById("id_description").value;
    const exp_date = document.getElementById("id_exp_date").value;
    const price = document.getElementById("id_price").value;

    fetch(`/api/products/${id}/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "PUT",
        body: JSON.stringify({
            name: name,
            description: description,
            exp_date: exp_date,
            price: price
        })
    })
        .then(response => {
            console.log(response)
            if (response.ok) {
                alert('Product updated successfully');
                window.location.replace('../../');
            } else {
                alert("ERROR: invalid product");
            }
        })
        .catch(error => {
            console.error({"error": error})
            alert("ERROR: " + error)
        })
}