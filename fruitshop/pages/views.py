from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404

from django.urls import reverse
from django.views import View

from products.forms import ProductForm
from products.models import Product
from sells.forms import SellForm
from sells.models import Sell


def home_page(request):
    """
    This view is responsible for the home page.
    If the user is not authenticated, it shows urls for registration and login pages.
    If the user is authenticated, it shows urls for products and sells management pages (and logout).
    """
    is_auth = request.user.is_authenticated
    if is_auth:
        return render(request, 'home.html', {'is_auth': is_auth,
                                             'user_name': request.user.username,
                                             'manage_products_url': reverse('pages-management-products'),
                                             'manage_sells_url': reverse('pages-management-sells'),
                                             'logout_url': reverse('users-logout')})
    else:
        return render(request, 'home.html', {'is_auth': is_auth,
                                             'register_url': reverse('users-register'),
                                             'login_url': reverse('users-login')})


class ProductManageView(LoginRequiredMixin, View):
    """
    This view is responsible for the products management page.
    It shows a table of the products in the platform, ordered by id in ascending order.
    Users can delete products.
    Users can create and edit products by going to each respective url.
    """
    login_url = '/users/login/'

    def get(self, request):
        products = Product.objects.order_by('id')
        return render(request, 'manage-products.html', {'products': products,
                                                        'home_page_url': reverse('pages-home'),
                                                        'create_product_url': reverse('pages-management-products-create')})


class ProductCreateView(LoginRequiredMixin, View):
    """
    This view is responsible for the products creation page.
    It shows a form where users can create a product.
    The form calls the platform API to create the product.
    """
    login_url = '/users/login/'

    def get(self, request):
        product_form = ProductForm()
        return render(request, 'create-edit-product.html', {'is_create': True,
                                                            'products_management_url': reverse('pages-management-products'),
                                                            'product_form': product_form})


class ProductEditView(LoginRequiredMixin, View):
    """
    This view is responsible for the products update page.
    It shows a form where users can update a product.
    The form calls the platform API to update the product.
    """

    login_url = '/users/login/'

    def get(self, request, product_id):
        product = get_object_or_404(Product, pk=product_id)
        product_form = ProductForm(
            initial={'name': product.name, 'description': product.description, 'exp_date': product.exp_date,
                     'price': product.price})
        return render(request, 'create-edit-product.html', {'is_create': False,
                                                            'products_management_url': reverse('pages-management-products'),
                                                            'product_id': product_id,
                                                            'product_form': product_form})


class SellManageView(LoginRequiredMixin, View):
    """
    This view is responsible for the sells management page.
    It shows a table of the sells in the platform, ordered by id in ascending order.
    Users can delete sells.
    Users can create and edit sells by going to each respective url.
    """
    login_url = '/users/login/'

    def get(self, request):
        sells = Sell.objects.order_by('id')
        return render(request, 'manage-sells.html', {'sells': sells,
                                                     'home_page_url': reverse('pages-home'),
                                                     'create_sell_url': reverse('pages-management-sells-create')})


class SellCreateView(LoginRequiredMixin, View):
    """
    This view is responsible for the sells creation page.
    It shows a form where users can create a sell.
    The form calls the platform API to create the sell.
    """
    login_url = '/users/login/'

    def get(self, request):
        sell_form = SellForm(initial={'user': request.user})
        return render(request, 'create-edit-sell.html', {'is_create': True,
                                                         'sells_management_url': reverse('pages-management-sells'),
                                                         'sell_form': sell_form})


class SellEditView(LoginRequiredMixin, View):
    """
    This view is responsible for the sells update page.
    It shows a form where users can update a sell.
    The form calls the platform API to update the sell.
    """
    login_url = '/users/login/'

    def get(self, request, sell_id):
        sell = get_object_or_404(Sell, pk=sell_id)
        sell_form = SellForm(
            initial={'product': sell.product, 'user': sell.user, 'quantity': sell.quantity})
        return render(request, 'create-edit-sell.html', {'is_create': False,
                                                         'sells_management_url': reverse('pages-management-sells'),
                                                         'sell_id': sell_id,
                                                         'sell_form': sell_form})
