from django.urls import path

from . import views
from .views import ProductManageView, ProductCreateView, ProductEditView, SellManageView, SellCreateView, SellEditView

urlpatterns = [
    path('', views.home_page, name='pages-home'),
    path('management/products/', ProductManageView.as_view(), name='pages-management-products'),
    path('management/products/add-edit/', ProductCreateView.as_view(), name='pages-management-products-create'),
    path('management/products/add-edit/<int:product_id>/', ProductEditView.as_view(), name='pages-management-products-edit'),
    path('management/sells/', SellManageView.as_view(), name='pages-management-sells'),
    path('management/sells/add-edit/', SellCreateView.as_view(), name='pages-management-sells-create'),
    path('management/sells/add-edit/<int:sell_id>/', SellEditView.as_view(), name='pages-management-sells-edit')
]
