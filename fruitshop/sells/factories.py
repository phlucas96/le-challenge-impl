import datetime

import factory
from factory.fuzzy import FuzzyText, FuzzyDate, FuzzyInteger

from products.factories import ProductFactory
from sells.models import Sell
from users.factories import UserFactory


class SellFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Sell

    user = factory.SubFactory(UserFactory)
    product = factory.SubFactory(ProductFactory)
    quantity = FuzzyInteger(low=1)
