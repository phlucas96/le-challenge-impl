from django import forms

from sells.models import Sell


class SellForm(forms.ModelForm):
    quantity = forms.IntegerField(label_suffix=" - integer")

    class Meta:
        model = Sell
        fields = ('product', 'user', 'quantity')
