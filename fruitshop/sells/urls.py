from django.urls import path

from .views import SellsListView, SellsDetailView

urlpatterns = [
    path('', SellsListView.as_view(), name='sells-list'),
    path('<int:sell_id>/', SellsDetailView.as_view(), name='sells-detail')
]
