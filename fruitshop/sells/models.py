from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, SuspiciousOperation
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.
from django.db.models import CASCADE

from products.models import Product


class Sell(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE, related_name='sells')
    product = models.ForeignKey(Product, on_delete=CASCADE, related_name='sells')
    quantity = models.IntegerField(validators=[
        MinValueValidator(1)
    ])

    def __str__(self):
        return f"{self.product.name}: {self.quantity} units - {self.user.username}"


def validate_sell(sell):
    try:
        product_id = sell["product_id"]
        user_id = sell["user_id"]
        quantity = int(sell["quantity"])
        return product_id, user_id, quantity
    except (KeyError, TypeError, ValueError):
        raise SuspiciousOperation('invalid body')


def sell_to_dict(s):
    return {'id': s.id, 'user': {'id': s.user.id, 'name': s.user.username},
            'product': {'id': s.product.id, 'name': s.product.name},
            'quantity': s.quantity}
