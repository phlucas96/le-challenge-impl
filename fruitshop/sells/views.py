import json
from json.decoder import JSONDecodeError

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404

# Create your views here.
from django.views import View
from django.forms.models import model_to_dict

from .models import Sell, validate_sell, sell_to_dict


class SellsListView(LoginRequiredMixin, View):

    def get(self, request):
        """
        Lists all the sells in the platform.
        :return: a list of sells.
        """
        sells = Sell.objects.all()
        sells_list = [sell_to_dict(s) for s in sells]
        return JsonResponse({'results': sells_list})

    def post(self, request):
        """
        Creates a new sell.

        The request body must be in JSON and contain the following fields:
        - user_id: integer
        - product_id: integer
        - quantity: integer

        :return: the created sell.
        """
        try:
            json_body = json.loads(request.body)
            product_id, user_id, quantity = validate_sell(json_body)
        except (JSONDecodeError, ValidationError):
            return HttpResponseBadRequest("Invalid JSON message")

        sell = Sell.objects.create(product_id=product_id, user_id=user_id, quantity=quantity)
        sell_dict = model_to_dict(sell)
        return JsonResponse(sell_dict, status=201)


class SellsDetailView(LoginRequiredMixin, View):

    def get(self, request, sell_id):
        """
        Retrieves a sell with a specific ID.
        :param sell_id: integer
        :return: a sell.
        """
        sell = get_object_or_404(Sell, pk=sell_id)
        sell_dict = sell_to_dict(sell)
        return JsonResponse(sell_dict)

    def put(self, request, sell_id):
        """
        Updates a sell identified by an ID.

        The request body must be in JSON and contain the following fields:
        - user_id: integer
        - product_id: integer
        - quantity: integer

        :param sell_id: integer
        :return: the updated sell.
        """
        try:
            json_body = json.loads(request.body)
            validate_sell(json_body)
        except (JSONDecodeError, ValidationError):
            return HttpResponseBadRequest("Invalid JSON message")

        get_object_or_404(Sell, pk=sell_id)

        Sell.objects.filter(id=sell_id).update(**json_body)

        sell = Sell.objects.get(pk=sell_id)
        sell_dict = model_to_dict(sell)
        return JsonResponse(sell_dict)

    def delete(self, request, sell_id):
        """
        Deletes a sell identified by an ID.

        :param sell_id: integer
        :return: nothing
        """
        sell = get_object_or_404(Sell, pk=sell_id)
        sell.delete()
        return HttpResponse(status=204)
