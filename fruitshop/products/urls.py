from django.urls import path

from .views import ProductsAPIListView, ProductsAPIDetailView

urlpatterns = [
    path('', ProductsAPIListView.as_view(), name='products-list'),
    path('<int:product_id>/', ProductsAPIDetailView.as_view(), name='products-detail'),
]
