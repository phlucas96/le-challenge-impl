import datetime

from django.core.exceptions import ValidationError, SuspiciousOperation
from django.db import models


# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=1024)
    exp_date = models.DateField()
    price = models.IntegerField()

    def __str__(self):
        return self.name


def validate_product(product):
    # TODO check types
    # throw 400
    try:
        name = product["name"]
        description = product["description"]
        exp_date = product["exp_date"]
        datetime.datetime.strptime(exp_date, '%Y-%m-%d')
        price = int(product["price"])
        return name, description, exp_date, price
    except (ValueError, KeyError, TypeError):
        raise SuspiciousOperation('invalid body')
