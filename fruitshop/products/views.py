import json
from json.decoder import JSONDecodeError

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render

# Create your views here.
from django.views import View
from django.forms.models import model_to_dict

from .forms import ProductForm
from .models import Product, validate_product


class ProductsAPIListView(LoginRequiredMixin, View):
    login_url = '/users/login/'

    def get(self, request):
        """
        Lists all the products in the platform.
        :return: a list of products.
        """
        products = list(Product.objects.all().values())
        return JsonResponse({'results': products})

    def post(self, request, **kwargs):
        """
        Creates a new product.

        The request body must be in JSON and contain the following fields:
        - name: string
        - description: string
        - exp_date: string in YYYY-MM-DD format
        - price: integer

        :return: the created product.
        """

        try:
            json_body = json.loads(request.body)
            name, description, exp_date, price = validate_product(json_body)
        except (JSONDecodeError, ValidationError):
            return HttpResponseBadRequest("Invalid JSON message")

        product = Product.objects.create(name=name, description=description, exp_date=exp_date, price=price)
        product_dict = model_to_dict(product)
        return JsonResponse(product_dict, status=201)


class ProductsAPIDetailView(LoginRequiredMixin, View):
    login_url = '/users/login/'

    def get(self, request, product_id):
        """
        Retrieves a product with a specific ID.
        :param product_id: integer
        :return: a product.
        """
        product = get_object_or_404(Product, pk=product_id)
        product_dict = model_to_dict(product)
        return JsonResponse(product_dict)

    def put(self, request, product_id):
        """
        Updates a product identified by an ID.

        The request body must be in JSON and contain the following fields:
        - name: string
        - description: string
        - exp_date: string in YYYY-MM-DD format
        - price: integer

        :param product_id: integer
        :return: the updated product.
        """
        try:
            json_body = json.loads(request.body)
            validate_product(json_body)
        except (JSONDecodeError, ValidationError):
            return HttpResponseBadRequest("Invalid JSON message")

        get_object_or_404(Product, pk=product_id)

        Product.objects.filter(id=product_id).update(**json_body)

        product = Product.objects.get(pk=product_id)
        product_dict = model_to_dict(product)
        return JsonResponse(product_dict)

    def delete(self, request, product_id):
        """
        Deletes a product identified by an ID.

        :param product_id: integer
        :return: nothing
        """
        product = get_object_or_404(Product, pk=product_id)
        product.delete()
        return HttpResponse(status=204)
