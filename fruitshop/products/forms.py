from django import forms

from products.models import Product


class ProductForm(forms.ModelForm):

    exp_date = forms.DateField(label="Expiration Date", label_suffix=" (YYYY-MM-DD)", input_formats=['%Y-%m-%d'])
    price = forms.IntegerField(label_suffix=" - integer")

    class Meta:
        model = Product
        fields = ('name', 'description', 'exp_date', 'price')
