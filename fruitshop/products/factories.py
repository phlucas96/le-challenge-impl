import datetime

import factory
from factory.fuzzy import FuzzyText, FuzzyDate, FuzzyInteger

from products.models import Product


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    name = FuzzyText()
    description = FuzzyText()
    exp_date = FuzzyDate(start_date=datetime.date.today(), end_date=(datetime.date.today() + datetime.timedelta(days=365)))
    price = FuzzyInteger(low=1)
