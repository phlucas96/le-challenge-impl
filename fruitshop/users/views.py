from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from users.forms import UserForm, LoginForm


class UsersRegisterClass(View):

    def get(self, request):
        """
        This view is responsible for register page.
        """
        is_successful = False
        user_form = UserForm()
        return render(request, 'register.html',
                      {'user_form': user_form, 'is_successful': is_successful, 'home_page_url': reverse('pages-home')})

    def post(self, request):
        """
        This view is responsible for receiving the data from the form of the register page.
        It allows users to register in the platform.
        """
        is_successful = False
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            is_successful = True

        return render(request, 'register.html',
                      {'user_form': user_form, 'is_successful': is_successful, 'home_page_url': reverse('pages-home')})


class UsersLoginClass(View):

    def get(self, request):
        """
        This view is responsible for login page.
        """
        login_form = LoginForm()
        return render(request, 'login.html', {'login_form': login_form, 'home_page_url': reverse('pages-home')})

    def post(self, request):
        """
        This view is responsible for receiving the data from the form of the login page.
        It allows users to login in the platform.
        """
        login_form = LoginForm(data=request.POST)

        if login_form.is_valid():
            user = authenticate(username=login_form.data["username"], password=login_form.data["password"])
            if user:
                login(request, user)
                return HttpResponseRedirect(reverse('pages-home'))
            else:
                error_message = 'Account is disabled'
        else:
            error_message = 'Invalid credentials'

        return render(request, 'login.html',
                      {'login_form': login_form, 'error_message': error_message, 'home_page_url': reverse('pages-home')})


class UsersLogoutClass(LoginRequiredMixin, View):

    # FIXME should be POST
    def get(self, request):
        """
        This view is responsible for the logout
        """
        logout(request)
        return HttpResponseRedirect(reverse('pages-home'))
