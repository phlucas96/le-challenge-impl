
import factory
from django.contrib.auth.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.LazyAttribute(lambda _: factory.Faker('safe_email'))
    password = factory.PostGenerationMethodCall('set_password', factory.Faker('password'))
    is_active = True
