from django.urls import path

from .views import UsersRegisterClass, UsersLoginClass, UsersLogoutClass

urlpatterns = [
    path('register/', UsersRegisterClass.as_view(), name='users-register'),
    path('login/', UsersLoginClass.as_view(), name='users-login'),
    path('logout/', UsersLogoutClass.as_view(), name='users-logout'),
]
